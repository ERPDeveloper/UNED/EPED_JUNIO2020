/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.EvalJun2020;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.StackIF;

/**
 * EPED Junio2019_2020.Class to manage stack.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 28 may 2020
 */
public class StackDeque<E> implements StackIF<E> {

    /**
     * Stack structure.
     *
     * @var Deque
     */
    private final Deque<E> stack;

    /**
     * Class constructor
     */
    public StackDeque() {
        this.stack = new Deque<>();
    }

	/**
     * Remove all items of the stack.
     */
    @Override
    public void clear() {
        this.stack.clear();
    }

    /**
     * Return true if param e is in the stack.
     *
     * @param e
     * @return boolean
     */
    @Override
    public boolean contains(E e) {
        return this.stack.contains(e);
    }

	/**
     * Returns the item at the top of the stack.
	 * @Pre !isEmpty ();
     *
	 * @return E
     */
    @Override
    public E getTop() {
        return this.stack.getFront();
    }

    /**
     * Returns an iterator on the stack.
     *
     * @return IteratorIF
     */
    @Override
    public IteratorIF<E> iterator() {
        return this.stack.iterator();
    }

    /**
     * Indicates if the structure is empty.
     *
     * @return boolean
     */
    @Override
    public boolean isEmpty() {
        return this.stack.isEmpty();
    }

	/**
     * Remove the top item of the stack.
	 * Stack size decreased by one.
	 * @Pre !isEmpty ();
     */
    @Override
    public void pop() {
        this.stack.removeFront();
    }

	/**
     * Includes an item at the top of the stack.
     * Increase stack size by one.
     *
	 * @param elem
     */
    @Override
    public void push(E elem) {
        this.stack.insertFront(elem);
    }

    /**
     * Get the size of the stack.
     *
     * @return int
     */
    @Override
    public int size() {
        return this.stack.size();
    }
}
