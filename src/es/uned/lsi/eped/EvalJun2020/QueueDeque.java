/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.EvalJun2020;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.QueueIF;

/**
 * EPED Junio2019_2020.Class to manage deque.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 28 may 2020
 */
public class QueueDeque<E> implements QueueIF<E> {

    /**
     * Queue structure.
     *
     * @var Deque
     */
    private final Deque<E> deque;

    /**
     * Class constructor.
     */
    public QueueDeque() {
        this.deque = new Deque<>();
    }

	/**
     * Remove all items of the queue.
     */
    @Override
    public void clear() {
        this.deque.clear();
    }

    /**
     * Return true if param e is in the queue.
     *
     * @param e
     * @return boolean
     */
    @Override
    public boolean contains(E e) {
        return this.deque.contains(e);
    }

    /**
     * Remove the first item from the queue.
     * Queue size decreased by one.
     * @Pre !isEmpty();
     */
    @Override
    public void dequeue() {
        this.deque.removeFront();
    }

    /**
     * Includes an item at the end of the queue.
     * Increase queue size by one.
     * @param elem
     */
    @Override
    public void enqueue(E elem) {
        this.deque.insertBack(elem);
    }

    /**
     * Get first item of the queue.
	 * @Pre !isEmpty()
     *
     * @return E
     */
    @Override
    public E getFirst() {
        return this.deque.firstNode.getValue();
    }

    /**
     * Returns an iterator on the queue.
     *
     * @return IteratorIF
     */
    @Override
    public IteratorIF<E> iterator() {
        return this.deque.iterator();
    }

    /**
     * Indicates if the structure is empty.
     *
     * @return boolean
     */
    @Override
    public boolean isEmpty() {
        return this.deque.isEmpty();
    }

    /**
     * Get the size of the queue.
     *
     * @return int
     */
    @Override
    public int size() {
        return this.deque.size();
    }
}
