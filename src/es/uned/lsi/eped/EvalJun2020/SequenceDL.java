/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.EvalJun2020;

import es.uned.lsi.eped.DataStructures.Collection;
import es.uned.lsi.eped.DataStructures.IteratorIF;

/**
 * EPED Junio2019_2020.Class to manage doubly linked sequences.
 *
 * @author EPED UNED lsi Teaching team
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 15 mar 2020
 */
public abstract class SequenceDL<E> extends Collection<E> implements SequenceDLIF<E> {

    /**
     * Class to implements the sequence node structure.
     */
	protected class NodeSequence {

		private E value = null;
		private NodeSequence next = null;
		private NodeSequence prev = null;

		NodeSequence(E e) {
			this.value = e;
		}

		public NodeSequence getNext() {
			return this.next;
		}

        public NodeSequence getPrev() {
			return this.prev;
		}

		public E getValue() {
			return this.value;
		}

		public void setNext(NodeSequence n) {
			this.next = n;
		}

		public void setPrev(NodeSequence n) {
			this.prev = n;
		}

        public void setValue(E e) {
			this.value = e;
		}
	}

	/**
     * Class to implements an iterator for the sequence.
     */
	private class SequenceIterator implements IteratorIF<E> {

        /**
         * Reference to the current item within the iterator.
         *
         * @var NodeSequence
         */
		private NodeSequence currentNode;

        /**
         * Class constructor.
         */
		SequenceIterator(){
			this.currentNode = firstNode;
		}

        @Override
		public E getNext() {
			E elem = this.currentNode.getValue();
			this.currentNode = this.currentNode.getNext();
			return elem;
		}

        @Override
		public boolean hasNext() {
			return this.currentNode != null;
		}

        @Override
		public void reset() {
			this.currentNode = firstNode;
		}
	}

    /**
     * Reference to the first element of the sequence.
     *
     * @var NodeSequence
     */
	protected NodeSequence firstNode;

	/**
     * Class constructor.
     */
	public SequenceDL () {
		super();
		this.firstNode = null;
	}

	/**
     * Class constructor by copy.
     *
     * @param s (Object from copy)
     */
	public SequenceDL (SequenceDL<E> s) {
		this();
		if ( ! s.isEmpty() ) {
			this.size = s.size();
			NodeSequence nextNode = s.getFirstNode();
			NodeSequence prevNode = new NodeSequence(nextNode.getValue());
			this.firstNode = prevNode;

			while ( nextNode.getNext() != null ) {
				nextNode = nextNode.getNext();
				NodeSequence newNode = new NodeSequence(nextNode.getValue());
                newNode.setPrev(prevNode);
				prevNode.setNext(newNode);
				prevNode = newNode;
			}
		}
	}

	/**
     * Remove all items of the sequence.
     */
    @Override
	public void clear () {
		super.clear();
		this.firstNode = null;
	}

	/**
     * Check if the sequence has an item.
     *
     * @param e
     * @return boolean
     */
    @Override
	public boolean contains(E e) {
		NodeSequence node = this.firstNode;
		while(node != null){
			E next = node.getValue();
			if(next.equals(e)) {
				return true;
			}
			node = node.getNext();
		}
		return false;
	}

	/**
     * Get an iterator of the sequence.
     *
     * @return SequenceIterator
     */
    @Override
    public IteratorIF<E> iterator() {
    	return new SequenceIterator();
    }

	/**
     * Get the item into i position of the sequence.
	 * @Pre: 1 <= i <= size()
     *
     * @param i
     * @return NodeSequence
     */
	protected NodeSequence getNode(int i){
		NodeSequence node = this.firstNode;
		for ( int aux = 1 ; aux < i ; aux++ ) {
			node = node.getNext();
		}
		return node;
	}

	/**
     * Get the first item of the sequence.
     */
	private NodeSequence getFirstNode() {
		return this.firstNode;
	}
}
