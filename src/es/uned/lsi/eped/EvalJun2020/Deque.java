/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.EvalJun2020;

/**
 * EPED Junio2019_2020.Class to manage deque with doubly linked sequences.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 28 may 2020
 */
public class Deque<E> extends SequenceDL<E> implements DequeIF<E> {

    /**
     * Reference to the last item in the Queue.
     *
     * @var NodeSequence
     */
    protected NodeSequence lastNode;

    /**
     * Class constructor.
     */
    public Deque() {
        super();
        this.lastNode = null;
    }

	/**
     * Class constructor by copy.
     *
     * @param s (Deque from copy)
     */
    public Deque(Deque<E> s) {
        super();
        this.lastNode = (this.isEmpty())
            ? null
            : (NodeSequence) this.getNode(this.size);
    }

	/**
     * Remove all items of the queue.
     */
    @Override
    public void clear() {
        super.clear();
        this.lastNode = null;
    }

	/**
     * Get last item from Queue.
	 * @Pre: !isEmpty().
     *
     * @return E
     */
    @Override
    public E getBack() {
        return this.lastNode.getValue();
    }

	/**
     * Get first item from Queue.
	 * @Pre: !isEmpty().
     *
     * @return E
     */
    @Override
    public E getFront() {
        return this.firstNode.getValue();
    }

	/**
     * Insert a new item at the end of the queue.
     *
	 * @param e
     */
    @Override
    public void insertBack(E e) {
		NodeSequence newNode = new NodeSequence(e);
		if (isEmpty()) {
			this.firstNode = newNode;
		} else {
            newNode.setPrev(this.lastNode);
			this.lastNode.setNext(newNode);
		}
		this.lastNode = newNode;
		this.size++;
    }

	/**
     * Insert a new item at the begin of the queue.
     *
	 * @param e
     */
    @Override
    public void insertFront(E e) {
		NodeSequence newNode = new NodeSequence(e);
		if (isEmpty()) {
			this.lastNode = newNode;
		} else {
            newNode.setNext(this.firstNode);
			this.firstNode.setPrev(newNode);
		}
		this.firstNode = newNode;
		this.size++;
    }

	/**
     * Remove last item of the queue.
	 * @Pre: !isEmpty().
     */
    @Override
    public void removeBack() {
        NodeSequence prevNode = this.lastNode.getPrev();
        this.lastNode.setPrev(null);
        this.lastNode = prevNode;
        this.size--;

        if (this.isEmpty()) {
            this.firstNode = null;
        } else {
            this.lastNode.setNext(null);
        }
    }

	/**
     * Remove first item of the queue.
	 * @Pre: !isEmpty().
     */
    @Override
    public void removeFront() {
        NodeSequence nextNode = this.firstNode.getNext();
        this.firstNode.setNext(null);
        this.firstNode = nextNode;
        this.size--;

        if (this.isEmpty()) {
            this.lastNode = null;
        } else {
            this.firstNode.setPrev(null);
        }
    }
}
