/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.EvalJun2020;

/**
 * EPED Junio2019_2020.Class to manage list with point of interest.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 26 may 2020
 */
public class ListIP<E> extends SequenceDL<E> implements ListIPIF<E> {

    /**
     * Indicates the mode depending on the position of the pointer.
     */
    private static final int BETWEEN = 50;
    private static final int FIRST = 1;
    private static final int LAST = 99;

    /**
     * Reference to the current item in the list.
     *
     * @var NodeSequence
     */
    private NodeSequence currentNode;

    /**
     * Numeric position of the current item in the list.
     *
     * @var int
     */
    private int pointer;

    /**
     * Class constructor.
     */
    public ListIP() {
        super();
        this.currentNode = null;
        this.pointer = 1;
    }

	/**
     * Returns the value of the element at the pointer position.
	 * @Pre: 1 <= getPointer() <= size()
     *
     * @return E
	 */
    @Override
	public E getElem() {
        return this.currentNode.getValue();
    }

	/**
     * Returns the current pointer position.
     * (1 >= value <= size()+1)
     *
     * @return int
     */
    @Override
	public int getPointer() {
        return this.pointer;
    }

	/**
     * Insert an element at the current pointer position.
	 * @Pre: 1 <= getPointer() <= size()+1
	 * @Post: getElem() = elem
     *
	 * @param elem
	 */
    @Override
	public void insert(E elem) {
        NodeSequence newNode = new NodeSequence(elem);
        switch (this.modePointer(true)) {
            case FIRST:
                this.firstNode = newNode;
                newNode.setNext(this.currentNode);
                if (this.currentNode != null) {
                    this.currentNode.setPrev(newNode);
                }
                break;

            case LAST:
                newNode.setPrev(this.currentNode);
                this.currentNode.setNext(newNode);
                break;

            default:
                NodeSequence previous = this.currentNode.getPrev();
                previous.setNext(newNode);
                newNode.setPrev(previous);
                newNode.setNext(this.currentNode);
                this.currentNode.setPrev(newNode);
                break;
        }
        this.currentNode = newNode;
		this.size++;
    }

	/**
     * Move the pointer to the next position in the list.
	 * @Pre: 1 <= getPointer() <= size()
	 * @Post: 1 < getPointer() <= size()+1
	 */
    @Override
	public void moveNext() {
        if (this.pointer <= this.size) {
            this.pointer++;
        }

        if (this.pointer < this.size) {
            this.currentNode = this.currentNode.getNext();
        }
    }

	/**
     * Move the pointer to the previous position in the list.
	 * @Pre: 1 < getPointer() <= size()+1
	 * @Post: 1 <= getPointer() < size()
	 */
    @Override
	public void movePrev() {
        this.currentNode = this.currentNode.getPrev();
        this.pointer--;
    }

	/**
     * Move the pointer to a certain position.
	 * @Pre: 1 <= pos <= size()+1
	 * @Post: getPointer() = pos
     *
     * @param pos
	 */
    @Override
	public void moveTo(int pos) {
        if (pos < this.pointer) {
            this.moveToPrevious(pos);
            return;
        }

        this.moveToNext(pos);
    }

	/**
     * Remove the element from the current pointer position.
	 * @Pre 1 <= getPointer() <= size()
	 */
    @Override
	public void remove() {
        NodeSequence previous = this.currentNode.getPrev();
        NodeSequence next = this.currentNode.getNext();
        NodeSequence newCurrentNode = next;

        switch (this.modePointer(false)) {
            case FIRST:
                if (next == null) {
                    this.firstNode = null;
                } else {
                    next.setPrev(null);
                }
                break;

            case LAST:
                previous.setNext(null);
                newCurrentNode = previous;
                this.pointer--;
                break;

            default:
                previous.setNext(next);
                next.setPrev(previous);
                break;
        }
        this.currentNode.setPrev(null);
        this.currentNode.setNext(null);
        this.currentNode = newCurrentNode;
        this.size--;
    }

	/**
     * Modify the value of the element that is at the current pointer position.
	 * @Pre: 1 <= getPointer() <= size()
	 * @Post: getElem() = elem
     *
	 * @param elem
	 */
    @Override
	public void setElem(E elem) {
        this.currentNode.setValue(elem);
    }

    /**
     * Get pointer mode from pointer position.
     *
     * @param forInsert
     * @return int
     */
    private int modePointer(boolean forInsert) {
        if (this.pointer == 1) {
            return ListIP.FIRST;
        }

        if (forInsert && this.pointer > this.size) {
            return ListIP.LAST;
        }

        if (!forInsert && this.pointer == this.size) {
            return ListIP.LAST;
        }

        return ListIP.BETWEEN;
    }

	/**
     * Move the pointer to a later position.
     *
     * @param pos
	 */
    private void moveToNext(int pos) {
        while (this.pointer < pos) {
            if (this.pointer < this.size) {
                this.currentNode = this.currentNode.getNext();
            }
            this.pointer++;
        }
    }

	/**
     * Move the pointer to a previous position.
     * @Pre: pos > getPointer()
     *
     * @param pos
	 */
    private void moveToPrevious(int pos) {
        if (pos < 2) {
            this.currentNode = this.firstNode;
            this.pointer = 1;
            return;
        }

        while (this.pointer > pos) {
            this.currentNode = this.currentNode.getPrev();
            this.pointer--;
        }
    }
}
