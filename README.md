# EPED 2019-2020

Ejercicios de evaluación del curso 2019-2020 convocatoria de Junio.
Se plantean un total de 4 ejercicios, 2 de ellos prácticos con implementación en Java y 2 teóricos.
Además de una serie de preguntas sobre el desarrollo y de aplicación de la materia de la asignatura.

### Pre-requisitos

Desarrollado en Java con el JDK 12, Encoding UTF-8


## Autores

* **EPED UNED (lsi)** - *Interfaces e Implementación inicial*
* **José Antonio Cuello** - *Implementación final*

## Licencia

Este proyecto está bajo la Licencia (GNU General Public License v3.0) - mira el archivo [LICENSE.md](LICENSE.md) para detalles
